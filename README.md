##### Follow these steps to start using the bookstore.application:

1.Run 'docker-compose up'
then the application create db and insert some tranzactions in mysql db
if you don't have mysql db please check http://localhost:8580/?server=db&username=root (password : root)
db name is bookstore

2.default user 1 role publisher
{username : fuad , password: Fuad12}

default user 2 role user
{username : resad,password:Resad123}

3.first you must sign in and then this url return Bearer token witch you must use all request(/auth/** except)

4.add all controller for reviewer :

•AuthController
--http://localhost:8081/auth/signup                 (POST)
body {
"name": "name",
"surname": "surname",
"email": "email@gmail.com",
"username": "Name",
"password": "Name123"
}

--http://localhost:8081/auth/signing                 (POST)
body { "username": "Name", "password" : "Name123"}

•BookController
--http://localhost:8081/book/                       (get all book)
--http://localhost:8081/book/detail?bookId=1         (get book deail by bookId)
--http://localhost:8081/book/user-publisher?userId=1 (get list all books published by specific publisher)
--http://localhost:8081/book/search?name=             (search books by name)

•AuthorController
--http://localhost:8081/author/detail?authorId=1      (get author details)

•UserController
--http://localhost:8081/user/add/                     (POST , add new book bu user_publisher)
body  {
"name":"design patterns",
"description":"based patterns",
"price":10.8,
"bookType":"SPECIFIC",
"authorDto":{
"id":1
}
}

--http://localhost:8081/user/update/1             (PUT, update book witch added by me)
body{
{
"name": "design patterns",
"description": "UPDATE",
"price": 10.8,
"bookType": "SPECIFIC",
"authorDto": {
"id":1
}
}

