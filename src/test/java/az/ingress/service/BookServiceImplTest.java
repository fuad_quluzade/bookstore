package az.ingress.service;

import az.ingress.dao.BookRepository;
import az.ingress.dao.UserRepository;
import az.ingress.dto.BookDto;
import az.ingress.exception.ApiRequestException;
import az.ingress.model.Book;
import az.ingress.model.User;
import az.ingress.service.impl.BookServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest {

    private final Long id = 1234L;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private UserRepository userRepository;


    @BeforeEach
    void setUp() {
        bookService = new BookServiceImpl(bookRepository, userRepository, modelMapper);
    }

    @Test
    void canGetAllBook() {
        //Arrange
        Book book = mock(Book.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(book);

        BookDto bookDto = mock(BookDto.class);
        List<BookDto> bookDtoList = new ArrayList<>();
        bookDtoList.add(bookDto);
        when(bookRepository.findAll()).thenReturn(bookList);
        when(modelMapper.map(book, BookDto.class)).thenReturn(bookDto);

        //Act
        List<BookDto> actualBookDto = bookService.getBooks();

        //Assert
        assertEquals(bookDtoList, actualBookDto);
    }

    @Test
    public void testGetBookById() {
        //Arrange
        Book book = mock(Book.class);
        BookDto bookDto = mock(BookDto.class);

        when(bookRepository.findById(id)).thenReturn(Optional.ofNullable(book));
        when(modelMapper.map(book, BookDto.class)).thenReturn(bookDto);

        //Act
        BookDto dto = bookService.getBooksById(id);

        //Assert
        assertEquals(bookDto, dto);
    }

    @Test
    void ifBookDoesNotExistByIdThenThrowException() {
        //Arrange
        Book book = mock(Book.class);
        BookDto bookDto = mock(BookDto.class);
        when(bookRepository.findById(book.getId())).thenReturn(Optional.empty());

        //Act
        assertThatThrownBy(() -> bookService.getBooksById(book.getId()))
                .isInstanceOf(ApiRequestException.class)
                .hasMessage("Book with id:0 is not found.");
    }

    @Test
    void ifPublisherDoesNotExistByIdThenThrowException() {
        //Arrange
        User user=mock(User.class);
        when(userRepository.findById(user.getId())).thenReturn(Optional.empty());
        //Act
        assertThatThrownBy(()->bookService.findBooksByUser(user.getId()))
                .isInstanceOf(ApiRequestException.class)
                .hasMessage("User not found");

    }

}
