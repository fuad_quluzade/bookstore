package az.ingress.service;

import az.ingress.configuration.security.services.SecurityService;
import az.ingress.dao.AuthorRepository;
import az.ingress.dao.BookRepository;
import az.ingress.dao.UserRepository;
import az.ingress.dto.BookDto;
import az.ingress.exception.ApiRequestException;
import az.ingress.model.Book;
import az.ingress.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.cache.CacheManager;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private final Long id = 123L;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private UserServiceImpl userService;


    @Mock
    private UserRepository userRepository;

    private SecurityService securityService;

    private CacheManager cacheManager;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(bookRepository,modelMapper,authorRepository,userRepository,securityService,cacheManager);
    }

    @Test
    void ifBookWithSameIdThenThrowException(){
        Book book = mock(Book.class);
        BookDto bookDto = mock(BookDto.class);
        when(bookRepository.findById(book.getId())).thenReturn(Optional.of(book));
        assertThatThrownBy(() -> userService.addNewBook(bookDto))
                .isInstanceOf(ApiRequestException.class)
                .hasMessage("Book with same id present.");
    }

}
