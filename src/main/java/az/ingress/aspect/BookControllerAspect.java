package az.ingress.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class BookControllerAspect {

    @Before("execution(* az.ingress.controller.BookController.getAllBook())")
    public void before(JoinPoint joinPoint) {
        log.info("ASPECT :method start {} ",joinPoint.getSignature().getName());
    }


    @Before("execution(* az.ingress.controller.BookController.getBooksDetailsById(..))"+"&& args(id,..)")
    public void beforeById(JoinPoint joinPoint,Object id) {
        log.info("ASPECT :method start {} request id for Book Details {}",joinPoint.getSignature().getName(),id);
    }

    @Before("execution(* az.ingress.controller.BookController.findBooksByPublisher(..))"+"&& args(id,..)")
    public void beforeByPublisher(JoinPoint joinPoint,Object id) {
        log.info("ASPECT :method start {} request id for find Books by user {}",joinPoint.getSignature().getName(),id);
    }

    @AfterReturning(value = "execution(* az.ingress.controller.BookController.*(..))",returning = "response")
    public void after(JoinPoint joinPoint,Object response){
        log.info("ASPECT :method finish {} response - {}",joinPoint.getSignature().getName(),response);
    }
}
