package az.ingress.aspect;

import az.ingress.dto.AuthorDto;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LogAspect {

    @Before("execution(* az.ingress.controller.AuthController.*(..))"+"&& args(dto,..)")
    public void before(JoinPoint joinPoint,Object dto) {
        log.info("ASPECT :method start {} request - {}",joinPoint.getSignature().getName(),dto);
    }

    @AfterReturning(value = "execution(* az.ingress.controller.AuthController.*(..))",returning = "response")
    public void after(JoinPoint joinPoint,Object response){
        log.info("ASPECT :method finish {} response - {}",joinPoint.getSignature().getName(),response);
    }

    @Before("execution(* az.ingress.controller.AuthorController.*(..))"+"&& args(authorId,..)")
    public void beforeAuthor(JoinPoint joinPoint,Object authorId) {
        log.info("ASPECT :method start {} get request by id - {}",joinPoint.getSignature().getName(),authorId);
    }

    @AfterReturning(value = "execution(* az.ingress.controller.AuthorController.*(..))",returning="response")
    public void afterAuthor(JoinPoint joinPoint, AuthorDto response) {
        log.info("ASPECT :method finish {} response - {}",joinPoint.getSignature().getName(),response);
    }



}
