package az.ingress.dao;

//import az.ingress.constants.BookType;
import az.ingress.constants.BookType;
import az.ingress.dto.BookDto;
import az.ingress.model.Book;
import az.ingress.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface BookRepository extends JpaRepository<Book,Long> {

    @Query("select b.id,b.name,b.description,b.bookType from Book b where b.name  like  %?1%")
    Page<Book> search(String name, Pageable pageable);

    Optional<List<Book>> findBookByUser(User user);


}
