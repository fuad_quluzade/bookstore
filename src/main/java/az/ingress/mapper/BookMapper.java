package az.ingress.mapper;

import az.ingress.dto.BookDto;
import az.ingress.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BookMapper {


    BookDto entityToDto(Book book);


    Book dtoToEntity(BookDto bookdto);
}
