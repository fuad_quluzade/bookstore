package az.ingress.mapper;

import az.ingress.dto.UserDto;
import az.ingress.model.User;

public interface UserMapper {

    UserDto entityToDto(User user);
}
