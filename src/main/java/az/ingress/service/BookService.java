package az.ingress.service;

import az.ingress.dto.BookDto;
import az.ingress.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    List<BookDto> getBooks();
    Page<Book> findByName(String name, Pageable pageable);
    BookDto getBooksById(Long id);
    List<BookDto> findBooksByUser(Long userId);
}
