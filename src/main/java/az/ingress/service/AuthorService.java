package az.ingress.service;

import az.ingress.dto.AuthorDto;

public interface AuthorService {
    AuthorDto getAuthorById(Long id);
}
