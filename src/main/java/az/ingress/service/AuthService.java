package az.ingress.service;


import az.ingress.dto.request.LoginRequest;
import az.ingress.dto.request.SignupRequest;
import az.ingress.dto.response.ApiResponse;
import az.ingress.dto.response.JwtResponse;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;

public interface AuthService {

    JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response);
    ApiResponse registerUser(@RequestBody SignupRequest signUpRequest);
}
