package az.ingress.service;

import az.ingress.dto.BookDto;


public interface UserService {
    void addNewBook(BookDto bookDto);
    void updateBookByPublisher(Long id, BookDto bookDto);

}
