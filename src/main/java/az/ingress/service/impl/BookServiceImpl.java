package az.ingress.service.impl;

import az.ingress.dao.BookRepository;
import az.ingress.dao.UserRepository;
import az.ingress.dto.AuthorDto;
import az.ingress.dto.BookDto;
import az.ingress.dto.UserDto;
import az.ingress.exception.ApiRequestException;
import az.ingress.model.Book;
import az.ingress.model.User;
import az.ingress.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;


    @Override
    @Cacheable(cacheNames = "book")
    public List<BookDto> getBooks() {
       List<Book> books=  bookRepository.findAll();
//     List<BookDto> bookDtos=new ArrayList<>();
//       for (Book book : books) {
//           UserDto userDto=new UserDto();
//           userDto.setId(book.getUser().getId());
//           AuthorDto authorDto=new AuthorDto();
//           authorDto.setId(book.getAuthor().getId());
//           BookDto bookDto = new BookDto();
//           bookDto.setId(book.getId());
//           bookDto.setName(book.getName());
//           bookDto.setDescription(book.getDescription());
//           bookDto.setUserDto(userDto);
//           bookDto.setAuthorDto(authorDto);
//           bookDtos.add(bookDto);
//       }
//        return bookDtos;
       return convertToBookDTOList(books);
    }

    @Override
    public Page<Book> findByName(String name, Pageable pageable) {
        return bookRepository.search(name,pageable);
    }

    @Override
    @Cacheable(cacheNames = "bookDto",key="#id")
    public BookDto getBooksById(Long id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ApiRequestException("Book with id:" + id + " is not found."));
        BookDto bookDto= modelMapper.map(book,BookDto.class);
        bookDto.setAuthorDto(modelMapper.map(book.getAuthor(),AuthorDto.class));
        bookDto.setUserDto(modelMapper.map(book.getUser(),UserDto.class));
        return bookDto;
    }

    @Override
    public List<BookDto> findBooksByUser(Long userId) {
        User user=userRepository.findById(userId)
                .orElseThrow(() -> new ApiRequestException("User not found"));
        List<Book> book = bookRepository.findBookByUser(user).orElse(Collections.emptyList());
        return convertToBookDTOList(book);
    }

    private List<BookDto> convertToBookDTOList(List<Book> books) {
        return books.stream()
                .map(book -> modelMapper.map(book, BookDto.class))
                .collect(Collectors.toList());
    }
}
