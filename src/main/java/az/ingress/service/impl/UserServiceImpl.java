package az.ingress.service.impl;

import az.ingress.configuration.security.services.SecurityService;
import az.ingress.constants.BookType;
import az.ingress.dao.AuthorRepository;
import az.ingress.dao.BookRepository;
import az.ingress.dao.UserRepository;
import az.ingress.dto.AuthorDto;
import az.ingress.dto.BookDto;
import az.ingress.dto.UserDto;
import az.ingress.exception.ApiRequestException;
import az.ingress.model.Author;
import az.ingress.model.Book;
import az.ingress.model.User;
import az.ingress.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;
    private final AuthorRepository authorRepository;
    private final UserRepository userRepository;
    private final SecurityService securityService;

    private final CacheManager cacheManager;


    @Override
    public void addNewBook(BookDto bookDto) {
        log.info("Request  for add bookDto name: "  +bookDto.getName());
        if (bookDto.getId() != null) {
            Optional<Book> bookById = bookRepository.findById(bookDto.getId());
            bookById.ifPresent(book -> {
                throw new ApiRequestException("Book with same id present.");
            });
        }
        Long user_publisher=currentUserLogin();
        Author author = authorRepository.findById(bookDto.getAuthorDto().getId())
                .orElseThrow(() -> new ApiRequestException("Author not found"));
        User user=userRepository.findById(user_publisher)
                .orElseThrow(() -> new ApiRequestException("User not found "));
        bookDto.setUserDto(modelMapper.map(user, UserDto.class));
        bookDto.setAuthorDto(modelMapper.map(author, AuthorDto.class));
        Book book = modelMapper.map(bookDto, Book.class);
        bookRepository.save(book);

        Cache cache=cacheManager.getCache("book");
        assert cache != null;
        cache.clear();
    }


    @Override
    public void updateBookByPublisher(Long id, BookDto bookDto) {
        log.info("Request for update  book bookId : "  +id);
        Long user_publisher=currentUserLogin();
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ApiRequestException("Book with id:" + id + " is not found."));
        Author author=authorRepository.findById(bookDto.getAuthorDto().getId())
                .orElseThrow(()-> new ApiRequestException("Author with id :" + id+ "not found"));
        if(book.getUser().getId().equals(user_publisher)) {
            book.setId(id);
            book.setName(bookDto.getName());
            book.setPrice(bookDto.getPrice());
            book.setDescription(bookDto.getDescription());
            book.setBookType(BookType.valueOf(bookDto.getBookType().name()));
            book.setAuthor(author);
            bookRepository.save(book);
            Cache cache=cacheManager.getCache("book");
            assert cache != null;
            cache.clear();
            Cache cache1=cacheManager.getCache("bookDto"+"::"+bookDto.getId());
            System.out.println(cache1.getName());
            assert cache1 != null;
            cache1.put(bookDto,bookDto.getId());


        }else{
            throw new ApiRequestException("This book is not published by me");
        }
    }

     private Long currentUserLogin(){
        String login = securityService.getCurrentUserLogin()
                .orElseThrow(() -> new ApiRequestException("user not found"));
        User user_publisher=userRepository.findByUsername(login)
                .orElseThrow(() -> new ApiRequestException("user_publisher not found "));
        return user_publisher.getId();
    }

}
