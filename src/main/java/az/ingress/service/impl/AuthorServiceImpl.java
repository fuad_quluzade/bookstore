package az.ingress.service.impl;

import az.ingress.dao.AuthorRepository;
import az.ingress.dto.AuthorDto;
import az.ingress.exception.ApiRequestException;
import az.ingress.model.Author;
import az.ingress.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final ModelMapper modelMapper;

    @Override
    public AuthorDto getAuthorById(Long id) {
        Author author = authorRepository.findById(id)
                .orElseThrow(() -> new ApiRequestException("Author with id:" + id + " is not found."));
        AuthorDto dto =
                modelMapper.map(author, AuthorDto.class);
        return dto;
    }
}
