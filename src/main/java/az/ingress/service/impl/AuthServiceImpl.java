package az.ingress.service.impl;


import az.ingress.configuration.security.jwt.JwtUtils;
import az.ingress.configuration.security.services.UserDetailsImpl;
import az.ingress.dao.RoleRepository;
import az.ingress.dao.UserRepository;
import az.ingress.dto.request.LoginRequest;
import az.ingress.dto.request.SignupRequest;
import az.ingress.dto.response.ApiResponse;
import az.ingress.dto.response.JwtResponse;
import az.ingress.model.Role;
import az.ingress.model.User;
import az.ingress.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final RoleRepository roleRepository;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    private static final String USER = "ROLE_USER";

    @Override
    public JwtResponse authenticateUser(LoginRequest loginRequest, HttpServletResponse response) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        JwtResponse jwtResponse = JwtResponse.builder()
                .token(jwt)
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .email(userDetails.getEmail())
                .roles(roles)
                .build();
        return jwtResponse;
    }

    @Override
    public ApiResponse registerUser(@Valid SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ApiResponse("Exists by username");
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ApiResponse("Exists by email");
        }
         User user = User.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .firstName(signUpRequest.getName())
                .lastName(signUpRequest.getSurname())
                .password(encoder.encode(signUpRequest.getPassword()))
                .build();
        Role userRole = roleRepository.findByName(USER)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        user.setRole(userRole);
        userRepository.save(user);
        return new ApiResponse("Successfully registered");
    }
}

