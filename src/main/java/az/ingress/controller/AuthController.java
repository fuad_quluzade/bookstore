package az.ingress.controller;

import az.ingress.dto.request.LoginRequest;
import az.ingress.dto.request.SignupRequest;
import az.ingress.dto.response.ApiResponse;
import az.ingress.dto.response.JwtResponse;
import az.ingress.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
@Slf4j
@Api("Authentication controller")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/signing")
    @ApiOperation(value = "Login with username and password")
    public JwtResponse authenticateUser(@Valid  @RequestBody LoginRequest loginRequest,
                                        @NotNull HttpServletResponse response) {
        return authService.authenticateUser(loginRequest, response);

    }

    @PostMapping("/signup")
    @ApiOperation(value = "New User registration")
    public ApiResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return authService.registerUser(signUpRequest);

    }
}
