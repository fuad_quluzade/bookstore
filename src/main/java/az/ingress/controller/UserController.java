package az.ingress.controller;

import az.ingress.dto.BookDto;
import az.ingress.dto.response.ApiResponse;
import az.ingress.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService publisherService;



    @PostMapping("/add")
    public ResponseEntity<ApiResponse> addNewBook(@RequestBody BookDto bookDto){
        publisherService.addNewBook(bookDto);
        ApiResponse response = new ApiResponse();
        response.setMessage("New Book Added Successfully!");
        return new ResponseEntity<ApiResponse>(response, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateBookByPublisher(@PathVariable Long id,@RequestBody BookDto bookDto){
          publisherService.updateBookByPublisher(id,bookDto);
          ApiResponse response=new ApiResponse();
          response.setMessage("Update successfully!");
          return new ResponseEntity<>(response,HttpStatus.OK);
    }
}
