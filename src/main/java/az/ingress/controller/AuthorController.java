package az.ingress.controller;

import az.ingress.dto.AuthorDto;
import az.ingress.model.Author;
import az.ingress.service.AuthorService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.service.Contact;

@RestController
@RequiredArgsConstructor
@RequestMapping("/author")
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping("/detail")
    @ApiOperation(value = "Find author details by id",
            notes = "Provide an id  to look up specific author details from the authors",
            response = AuthorDto.class)
    public AuthorDto getAuthorDetailsById(@RequestParam Long authorId) {
        return authorService.getAuthorById(authorId);
    }
}
