package az.ingress.controller;


import az.ingress.dto.BookDto;
import az.ingress.model.Book;
import az.ingress.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public ResponseEntity<List<BookDto>> getAllBook(){
        List<BookDto> book= bookService.getBooks();
        return new ResponseEntity<List<BookDto>>(book,HttpStatus.OK);
    }


    @GetMapping("/search")
    public Page<Book> findBySearchingAndPagination(@RequestParam Optional<String> name,
                                                   @RequestParam Optional<Integer> page,
                                                   @RequestParam Optional<Integer> size,
                                                   @RequestParam Optional<String> sortBy){
        return bookService.findByName(name.orElse("_"),
                PageRequest.of(page.orElse(0), size.orElse(2),Sort.Direction.ASC,sortBy.orElse("id")));
    }

    @GetMapping("/detail")
    public ResponseEntity<BookDto> getBooksDetailsById(@RequestParam Long bookId) {
        BookDto bookDto= bookService.getBooksById(bookId);
        return new ResponseEntity<BookDto>(bookDto,HttpStatus.OK);
    }


    @GetMapping("/user-publisher")
        public ResponseEntity<List<BookDto>> findBooksByPublisher(@RequestParam Long userId){
        List<BookDto> book=bookService.findBooksByUser(userId);
        return new ResponseEntity<List<BookDto>>(book,HttpStatus.OK);

    }
}
