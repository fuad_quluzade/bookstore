package az.ingress.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Serializable {

    static final long serialVersionUID = 687991492884005036L;
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String username;
    private AuthorDto authorDto;
    private RoleDto role;
}
