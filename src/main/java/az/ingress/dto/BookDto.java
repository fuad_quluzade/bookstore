package az.ingress.dto;

//import az.ingress.constants.BookType;
import az.ingress.constants.BookType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class BookDto implements Serializable {

    static final long serialVersionUID = 687991492884005034L;

    private Long id;
    private String name;
    private String description;
    private Double price;
    private BookType bookType;
//    @JsonIgnore
    @ToString.Exclude
    private UserDto userDto;
//    @JsonIgnore
    @ToString.Exclude
    private AuthorDto authorDto;

}
