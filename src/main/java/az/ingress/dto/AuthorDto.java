package az.ingress.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Details about the author")
public class AuthorDto implements Serializable {
    static final long serialVersionUID = 687991492884005035L;

    private Long id;
    @ApiModelProperty(notes = "authors name")
    private String name;
    private String surname;
    private String birthday;
    private String motherland;
    private BookDto bookDto;
}
