package az.ingress.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto implements Serializable {
    static final long serialVersionUID = 687991492884005037L;
    private Long id;
    private String name;
}
