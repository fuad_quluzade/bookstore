package az.ingress.model;

//import az.ingress.constants.BookType;
import az.ingress.constants.BookType;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Table(name = "book")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Book {

    static final long serialVersionUID = 687991492884005044L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Double price;
    @Enumerated(EnumType.STRING)
    private BookType bookType;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id",updatable = false)
    @ToString.Exclude
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id",updatable = false)
    @ToString.Exclude
    private Author author;




}
