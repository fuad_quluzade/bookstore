package az.ingress.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(ApiRequestException.class )
    public ResponseEntity<Object> handleApiRequestExceptions(ApiRequestException e){
        HttpStatus notFound=HttpStatus.NOT_FOUND;
          ApiException apiException = new ApiException(
                   e.getMessage(),
                   HttpStatus.NOT_FOUND,
                   ZonedDateTime.now()
           );
           return new ResponseEntity<>(apiException,notFound);
    }
}
